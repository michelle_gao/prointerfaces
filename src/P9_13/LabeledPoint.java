package P9_13;

import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: Michelle
 * Date: 10/25/13
 * Time: 10:20 PM
 * To change this template use File | Settings | File Templates.
 */
public class LabeledPoint {
    private String mLable;
    private Point mPoint;

    public LabeledPoint(int x, int y, String label) {
        mPoint = new Point(x, y);
        mLable = label;
    }

    @Override
    public String toString() {
        return "LabeledPoint{" +
                "mLable='" + mLable + '\'' +
                ", mPoint=" + mPoint +
                '}';
    }
}
