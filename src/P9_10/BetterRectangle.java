package P9_10;


import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: Michelle
 * Date: 10/25/13
 * Time: 9:34 PM
 * To change this template use File | Settings | File Templates.
 */
public class BetterRectangle extends Rectangle {
    public BetterRectangle(int x, int y, int width, int height) {
        setLocation(x, y);
        setSize(width, height);
    }

    public double getPerimeter() {
        return 2 * (width + height);
    }
    public double getArea() {
        return width*height;
    }
}
