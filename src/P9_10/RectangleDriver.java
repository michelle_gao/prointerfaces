package P9_10;

/**
 * Created with IntelliJ IDEA.
 * User: Michelle
 * Date: 10/25/13
 * Time: 10:12 PM
 * To change this template use File | Settings | File Templates.
 */
public class RectangleDriver {
    public static void main(String[] args) {
        int x = 100;
        int y = 233;
        int width = 2000;
        int height = 3000;
        BetterRectangle mBetterRectangle = new BetterRectangle(x, y, width, height);
        System.out.println("the perimeter of the rectangle is: " + mBetterRectangle.getPerimeter());
        System.out.println("the area of the rectangle is:" + mBetterRectangle.getArea());
    }
}
