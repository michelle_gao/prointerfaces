package P9_14;

import javax.swing.text.html.HTMLDocument;

/**
 * Created with IntelliJ IDEA.
 * User: Michelle
 * Date: 10/25/13
 * Time: 10:54 PM
 * To change this template use File | Settings | File Templates.
 */
public class SodaCanDriver {
    public static void main(String[] args) {
        Measurable[] sodaCan = { new SodaCan(12.5,60),new SodaCan(34,20),new SodaCan(33.3, 20.9)};
        double totalArea = 0;
        for (int i = 0; i < sodaCan.length; i++) {
            totalArea += sodaCan[i].measure();
        }
        System.out.println("Average Area of the total soda Cans is : " + totalArea / sodaCan.length);
    }
}
