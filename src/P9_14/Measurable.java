package P9_14;

/**
 * Created with IntelliJ IDEA.
 * User: Michelle
 * Date: 10/25/13
 * Time: 10:45 PM
 * To change this template use File | Settings | File Templates.
 */
public interface Measurable {
    public double measure();
}
