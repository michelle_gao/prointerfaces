package P9_14;

/**
 * Created with IntelliJ IDEA.
 * User: Michelle
 * Date: 10/25/13
 * Time: 10:51 PM
 * To change this template use File | Settings | File Templates.
 */
public class SodaCan implements Measurable{
    private  double dHeight;
    private  double dRadius;

    public SodaCan(double dHeight, double dRadius) {
        this.dHeight = dHeight;
        this.dRadius = dRadius;
    }

    @Override
    public double measure() {
        return Math.PI * dRadius* dRadius + 2* Math.PI *dRadius * dHeight;
    }
}
