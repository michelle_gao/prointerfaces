package P9_8;

/**
 * Created with IntelliJ IDEA.
 * User: Michelle
 * Date: 10/25/13
 * Time: 8:40 PM
 * To change this template use File | Settings | File Templates.
 */
public class Student extends Person {
    private String mMajor;

    public Student(String name, int year, String major) {
        super(name, year);
        mMajor = major;
    }

    @Override
    public String toString() {
        return "Student{" + "mName='" + super.getmName() + '\'' + " , mYear=" +super.getmYear()+
                " , mMajor='" + mMajor + '\'' +
                aChar();
    }

    private char aChar() {
        return '}';
    }
}
