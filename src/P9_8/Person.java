package P9_8;

/**
 * Created with IntelliJ IDEA.
 * User: Michelle
 * Date: 10/25/13
 * Time: 8:24 PM
 * To change this template use File | Settings | File Templates.
 */
public class Person {

    private String mName;
    private int mYear;
    public Person() {}
    public Person(String name, int year) {
        mName = name;
        mYear = year;
    }
    public String getmName() {
        return mName;
    }

    public  int getmYear() {
        return  mYear;
    }


    @Override
    public String toString() {
        return "Person{" +
                "mName='" + mName + '\'' +
                ", mYear=" + mYear +
                '}';
    }
}
