package P9_8;

/**
 * Created with IntelliJ IDEA.
 * User: Michelle
 * Date: 10/25/13
 * Time: 8:50 PM
 * To change this template use File | Settings | File Templates.
 */
public class Instructor extends Person {
    private double mSalary;

    public Instructor() {
    }

    public Instructor(String name, int year, double salary) {
        super(name, year);
        mSalary = salary;
    }


    @Override
    public String toString() {
        return "Instructor{" +  "mName='" + super.getmName() + '\''+ " , mYear=" + super.getmYear() +
                " , mSalary=" + mSalary +
                '}';
    }
}
