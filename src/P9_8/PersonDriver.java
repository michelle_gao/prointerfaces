package P9_8;

/**
 * Created with IntelliJ IDEA.
 * User: Michelle
 * Date: 10/25/13
 * Time: 8:56 PM
 * To change this template use File | Settings | File Templates.
 */
public class PersonDriver {
    public static void main(String[] args) {
        Person mPerson = new Person("Jennifer", 1999);
        Student mStudent = new Student("Michael", 1980, "CS");
        Instructor mInstructor = new Instructor("Garth", 1960, 100000);
        System.out.println(mPerson);
        System.out.println(mStudent);
        System.out.println(mInstructor);

    }
}
