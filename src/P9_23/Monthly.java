package P9_23;

import java.util.Calendar;

/**
 * Created with IntelliJ IDEA.
 * User: Michelle
 * Date: 10/27/13
 * Time: 2:55 PM
 * To change this template use File | Settings | File Templates.
 */
public class Monthly extends Appointment{
    public Monthly(String description, Calendar calendar) {
        super(description, calendar);
    }
    @Override
    public boolean occurSon(int year, int month, int day) {
        Calendar cal1 = Calendar.getInstance();
        cal1.set(year, month, day);
        Calendar cal2 = getmCalendar();
        if ( cal1.get(Calendar.DAY_OF_MONTH) ==
                cal1.get(Calendar.DAY_OF_MONTH)){
            return true;
        }
        else return false;
    }
}
