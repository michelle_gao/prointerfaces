package P9_23;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: Michelle
 * Date: 10/27/13
 * Time: 3:04 PM
 * To change this template use File | Settings | File Templates.
 */
public class AppointmentDriver {
    public static void main(String[] args) throws FileNotFoundException {
        PrintWriter out = new PrintWriter("appointmentfile.txt");
        ArrayList<Appointment> arrAppoint = new ArrayList<Appointment>();
        Calendar cal1= Calendar.getInstance();
        Calendar cal2= Calendar.getInstance();
        Calendar cal3= Calendar.getInstance();
        Calendar cal4= Calendar.getInstance();
        Calendar cal5= Calendar.getInstance();

        cal1.set(1999, 10, 5);
        arrAppoint.add(insertAppointment("See dentist", "Onetime",cal1));

        cal2.set(2000, 11, 6);
        arrAppoint.add(insertAppointment("see primary doctor","Monthly",cal2));

        cal3.set(2006, 7, 26);
        arrAppoint.add(insertAppointment("stand-up conference","Daily",cal3));

        cal4.set(2013, 6, 6);
        arrAppoint.add(insertAppointment("See Specialist","Monthly",cal4));

        cal5.set(2013, 8, 8);
        arrAppoint.add(insertAppointment("vacation","Onetime", cal5));

        for (int i = 0; i < arrAppoint.size(); i++)  {
            System.out.println(arrAppoint.get(i));
            out.println(arrAppoint.get(i));

        }
        out.close();

        File appointFile = new File("appointmentfile.txt");
        ArrayList<Appointment> inputAppoint = new ArrayList<Appointment>();
        Scanner sc = new Scanner(appointFile);
        String strLine;
        String[] strObject;
        Calendar[] calFromFile;
        calFromFile = new Calendar[100];
        int i=0;
        while (sc.hasNextLine()) {
           strLine = sc.nextLine();
           strObject = strLine.split(",");
           calFromFile[i] = Calendar.getInstance();
           calFromFile[i].set(Calendar.YEAR, Integer.parseInt(strObject[2]));
           calFromFile[i].set(Calendar.MONTH,Integer.parseInt(strObject[3]));
           calFromFile[i].set(Calendar.DAY_OF_MONTH,Integer.parseInt(strObject[4]));
            if (strObject[0].contains("Daily")) {
                inputAppoint.add(insertAppointment(strObject[1], "Daily", calFromFile[i]));
                System.out.println(insertAppointment(strObject[1], "Daily", calFromFile[i]));
            }
            if (strObject[0].contains("Onetime")) {
                inputAppoint.add(insertAppointment(strObject[1], "Onetime", calFromFile[i]));
                System.out.println(insertAppointment(strObject[1], "Onetime", calFromFile[i]));
            }
            if (strObject[0].contains("Monthly")) {
                inputAppoint.add(insertAppointment(strObject[1], "Monthly", calFromFile[i]));
                System.out.println(insertAppointment(strObject[1], "Monthly", calFromFile[i]));

            }
            i++;
        }

    }

    public static Appointment insertAppointment(String description, String appointType, Calendar cal) {
        Appointment insAppointment = null;
        switch (appointType) {
            case "Daily":
                return new Daily(description, cal);
            case "Monthly":
                return new Monthly(description, cal);
            case "Onetime":
                return new Onetime(description, cal);
            default:
                return null;
        }
    }

}
