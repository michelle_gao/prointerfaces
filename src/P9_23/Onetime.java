package P9_23;

import java.util.Calendar;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Michelle
 * Date: 10/27/13
 * Time: 2:18 PM
 * To change this template use File | Settings | File Templates.
 */
public class Onetime extends Appointment {
    public Onetime(String description, Calendar calendar) {
        super(description, calendar);
    }

    @Override
    public boolean occurSon(int year, int month, int day) {
        Calendar cal1 = Calendar.getInstance();
        cal1.set(year, month, day);
        Calendar cal2 = getmCalendar();
        if ( cal1.get(Calendar.YEAR) ==cal2.get(Calendar.YEAR) && cal1.get(Calendar.DAY_OF_YEAR) ==
        cal1.get(Calendar.DAY_OF_YEAR)){
            return true;
        }
        else return false;
    }
}
