package P9_23;

import java.util.Calendar;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Michelle
 * Date: 10/27/13
 * Time: 2:08 PM
 * To change this template use File | Settings | File Templates.
 */
public abstract class Appointment {
    private String mDescription;
    private Calendar mCalendar;
    public Appointment(){}

    public Appointment(String description, Calendar calendar) {
        mDescription=description;
        mCalendar = calendar;
    }

    public void setmDescription(String mDescription) {
        this.mDescription = mDescription;
    }

    public String getmDescription() {
        return mDescription;
    }

    public Calendar getmCalendar() {
        return mCalendar;
    }

    public void setmCalendar(Calendar mCalendar) {
        this.mCalendar = mCalendar;
    }

    public abstract boolean occurSon(int year, int month, int day);
    public String toString() {
        return this.getClass() + "," + getmDescription() + "," + getmCalendar().get(Calendar.YEAR) + ","
                + getmCalendar().get(Calendar.MONTH) + "," + getmCalendar().get(Calendar.DAY_OF_MONTH);
    }
}
