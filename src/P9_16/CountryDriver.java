package P9_16;

import P9_14.Measurable;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created with IntelliJ IDEA.
 * User: Michelle
 * Date: 10/27/13
 * Time: 1:25 PM
 * To change this template use File | Settings | File Templates.
 */
public class CountryDriver {
    public static void main(String[] args) {
        ArrayList<Measurable> meaCountries = new ArrayList<Measurable>();
        meaCountries.add(new Country("China", 9640011.00));
        meaCountries.add(new Country("United States of American", 9629091));
        meaCountries.add(new Country("Canada", 9699000));
        meaCountries.add(new Country("India", 3287590));
        meaCountries.add(new Country("Argentina", 2780400));
        Measurable[] arrCountries = new Measurable[meaCountries.size()];
        arrCountries = meaCountries.toArray(arrCountries);
        Measurable maxAreaCountry = maximum(arrCountries);
        System.out.print(maxAreaCountry);
    }

    public static Measurable maximum(Measurable[] objects) {
        double maximumArea = 0;
        Measurable maxAreaCountry = null;
        for (int i = 0; i < objects.length; i++) {
            if (objects[i].measure() > maximumArea){
                maximumArea = objects[i].measure();
                maxAreaCountry = objects[i];
            }
        }
        return maxAreaCountry;
    }
}
