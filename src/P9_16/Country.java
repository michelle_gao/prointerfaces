package P9_16;
import P9_14.Measurable;
/**
 * Created with IntelliJ IDEA.
 * User: Michelle
 * Date: 10/27/13
 * Time: 9:01 AM
 */
public class Country implements Measurable{
    private  double mArea;
    private  String mName;
    public Country(String name, double area) {
        mName = name;
        mArea = area;
    }
    @Override
    public double measure() {
        return mArea;
    }

    @Override
    public String toString() {
        return "The country with the largest area is: Country{" +
                "mArea=" + mArea +
                ", mName='" + mName + '\'' +
                '}';
    }
}
